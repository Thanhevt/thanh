<%--
  Created by IntelliJ IDEA.
  User: thu.phan
  Date: 10/16/17
  Time: 8:44 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Flowers for Dreams</title>
    <jsp:include page="head.jsp"></jsp:include>
</head>
<body>
<div class="footer">
    <div class="container">
        <div class="footer-grids">
            <div class="col-md-6 footer-grid animated wow slideInLeft" data-wow-delay=".5s">
                <h3>About Us</h3>
                <p>Create a "wow" moment with our flowers and gifts this holiday season.<span>Whether it is a thoughtful bouquet of birthday flowers or romantic anniversary flowers, we know fresh flowers and plants leave a lasting impression.</span></p>
            </div>
            <div class="col-md-6 footer-grid animated wow slideInLeft" data-wow-delay=".6s">
                <h3>Contact Info</h3>
                <ul>
                    <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>5th N Michigan Avenue, <span>Chicago, IL 60630.</span></li>
                    <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="mailto:info@example.com">info@example.com</a></li>
                    <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>+1 773 886 9848</li>
                </ul>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
    <div class="footer-logo animated wow slideInUp" data-wow-delay=".5s">
        <h2><a href="/">DREAMS <span>flowers shop</span></a></h2>
    </div>
    <div class="copy-right animated wow slideInUp" data-wow-delay=".5s">
        <p>&copy 2017 Dreams Flowers Shop. All rights reserved | Design by <a href="http://w3layouts.com/">Thanh Thu</a></p>
    </div>
</div>
</body>
</html>
