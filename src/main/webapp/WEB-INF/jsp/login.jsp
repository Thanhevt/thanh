<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<title>Flowers for Dreams | Login</title>
<jsp:include page="head.jsp"></jsp:include>
</head>
	
<body>
<!-- header -->
	<jsp:include page="header.jsp"></jsp:include>
<!-- //header -->
<!-- breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="/"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
				<li class="active">Login</li>
			</ol>
		</div>
	</div>
<!-- //breadcrumbs -->
<!-- login -->
	<div class="login">
		<div class="container" id="login">
			<h3 class="animated wow zoomIn" data-wow-delay=".5s">Login Form</h3>
			<p class="est animated wow zoomIn" data-wow-delay=".5s">Welcome, please enter the following to continue.</p>
            <div id="alert"></div>
			<div class="login-form-grids animated wow slideInUp" data-wow-delay=".5s">
				<form id ="user" onsubmit="login()">
					<input id="email" type="email" placeholder="Email Address" required="true" />
					<input id="password" type="password" placeholder="Password" required="true" />
					<input type="submit" value="Login">
				</form>
			</div>
			<h4 class="animated wow slideInUp" data-wow-delay=".5s">For New People</h4>
			<p class="animated wow slideInUp" data-wow-delay=".5s"><a href="/register">Register Here</a> (Or) go back to <a href="/">Home<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a></p>
		</div>
	</div>
    <div id="success"></div>

<!-- //login -->
<!-- footer -->
<jsp:include page="footer.jsp"></jsp:include>
<!-- //footer -->
<script>

    function login() {
        event.preventDefault();
        var loginURL = "/login";
        var obj = $("#email").val() + "," + $("#password").val();
        console.log("Test");
        $.ajax({
            type: 'POST',
            url: loginURL,
            contentType: 'application/json',
            data: obj,
            headers:
                {
                   acceptHeader: 'application/json'
                },
            traditional: true,
            timeout: 20000,
            success: function (response) {
                var alert = "<div class=\"alert alert-danger text-center\" role=\"alert\" id=\"alert\">" +
                    "<p>" + "<strong>" + "Invalid email or password!" + "</strong>" + "</p>" + "</div>";
                var activation = "<div class=\"alert alert-danger\" role=\"alert\" id=\"alert\">" + "<p>" + "<strong>" + "Your account has not been activated yet. Please check your email for account activation!" + "</strong>" + "</p>" + "</div>";
                console.log(response);
                if (response == "Error") {
                    $("#alert").empty();
					$("#alert").append(alert);
				} else if (response == "Activation") {
                    $("#alert").empty();
                    $("#alert").append(activation);
                } else {
					$(location).attr("href", "/");
                }
            },
            error: function (e) {
                console.log("ERROR: ", e);
            }
        });
    }
</script>
</body>
</html>