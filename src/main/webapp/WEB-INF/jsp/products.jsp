<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>Flowers for Dreams | Flowers</title>
   <jsp:include page="head.jsp"></jsp:include>
</head>

<body>
<!-- header -->
<jsp:include page="header.jsp"></jsp:include>
<!-- //header -->
<!-- breadcrumbs -->
<div class="breadcrumbs">
    <div class="container">
        <ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
            <li><a href="/"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
            <li class="active">${productList[0].category.name}</li>
        </ol>
    </div>
</div>
<!-- //breadcrumbs -->
<!--products-->
<div class="products">
    <div class="container">
        <div class = "col-md-4 products-left">
            <div class="new-products animated wow slideInUp" data-wow-delay=".5s">
                <h3>New Flowers</h3>
                <div class="new-products-grids">
                    <c:forEach var="product" items="${newProducts}">
                        <div class="new-products-grid">
                            <div class="new-products-grid-left">
                                <a href="/detail?id=${product.id}"><img src="/resources/images/newflowers/${product.photoList[0].path}" alt=" " class="img-responsive" /></a>
                            </div>
                            <div class="new-products-grid-right">
                                <h4><a href="/detail?id=${product.id}">${product.name}</a></h4>
                                <div class="rating">
                                    <div class="rating-left">
                                        <img src="/resources/images/2.png" alt=" " class="img-responsive">
                                    </div>
                                    <div class="rating-left">
                                        <img src="/resources/images/2.png" alt=" " class="img-responsive">
                                    </div>
                                    <div class="rating-left">
                                        <img src="/resources/images/2.png" alt=" " class="img-responsive">
                                    </div>
                                    <div class="rating-left">
                                        <img src="/resources/images/1.png" alt=" " class="img-responsive">
                                    </div>
                                    <div class="rating-left">
                                        <img src=/resources/"images/1.png" alt=" " class="img-responsive">
                                    </div>
                                    <div class="clearfix"> </div>
                                </div>
                                <div class="simpleCart_shelfItem new-products-grid-right-add-cart">
                                    <p> <span class="item_price">$1</span><a class="item_add" href="#">add to cart </a></p>
                                </div>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
            <div class="men-position animated wow slideInUp" data-wow-delay=".5s">
                <a href="single.html"><img src="/resources/images/4.jpg" alt=" " class="img-responsive" /></a>
                <div class="men-position-pos">
                    <h4>Summer collection</h4>
                    <h5><span>55%</span> Flat Discount</h5>
                </div>
            </div>
        </div>
        <div class="col-md-8 products-right">
            <div class="products-right-grids-bottom">
                <div class="new-collections-grids">
                    <c:forEach var="product" items="${productList}">
                        <div class="col-md-4 new-collections-grid">
                            <div class="new-collections-grid1 animated wow slideInUp" data-wow-delay=".5s">
                                <div class="new-collections-grid1-image">
                                    <a href="/detail?id=${product.id}" class="product-image"><img src="/resources/images/newflowers/${product.photoList[0].path}" alt=" " class="img-responsive" /></a>
                                    <div class="new-collections-grid1-image-pos">
                                        <a href="/detail?id=${product.id}">Quick View</a>
                                    </div>
                                    <div class="new-collections-grid1-right">
                                        <div class="rating">
                                            <div class="rating-left">
                                                <img src="/resources/images/2.png" alt=" " class="img-responsive" />
                                            </div>
                                            <div class="rating-left">
                                                <img src="/resources/images/2.png" alt=" " class="img-responsive" />
                                            </div>
                                            <div class="rating-left">
                                                <img src="/resources/images/2.png" alt=" " class="img-responsive" />
                                            </div>
                                            <div class="rating-left">
                                                <img src="/resources/images/1.png" alt=" " class="img-responsive" />
                                            </div>
                                            <div class="rating-left">
                                                <img src="/resources/images/1.png" alt=" " class="img-responsive" />
                                            </div>
                                            <div class="clearfix"> </div>
                                        </div>
                                    </div>
                                </div>
                                <h4><a href="/detail?id=${product.id}">${product.name}</a></h4>
                                <div class="new-collections-grid1-left simpleCart_shelfItem">
                                    <br><span class="item_price">$1</span></br><a class="item_add" href="#">add to cart </a>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
        <nav class="numbering animated wow slideInRight" data-wow-delay=".5s">
            <ul class="pagination paging"></ul>
        </nav>
    </div>
    <div class="clearfix"> </div>
</div>
<!-- //products -->
<!-- footer -->
<jsp:include page="footer.jsp"></jsp:include>
<!-- //footer -->>

<script type="text/javascript">
    $(function () {
        window.pagObj = $('.pagination').twbsPagination({
            totalPages: 3,
            visiblePages: 2
        }).on('page', function (event, page) {
            $.ajax({
                type : "POST",
                contentType : "application/json",
                url : "/category/page?id=1&page=" + page,
                //data : comment,
                timeout : 100000,
                success : function(data) {
                    display(data);
                }
            });
        });

        function display(data) {
            $(".col-md-4.new-collections-grid").html(data);
        }
    });
</script>

</body>
</html>