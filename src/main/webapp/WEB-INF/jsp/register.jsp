<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
    <title>Flowers for Dreams | Register</title>
    <jsp:include page="head.jsp"></jsp:include>
</head>

<body>
<!-- header -->
<jsp:include page="header.jsp"></jsp:include>
<!-- //header -->
<!-- breadcrumbs -->
<div class="breadcrumbs">
    <div class="container">
        <ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
            <li><a href="index.jsp"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
            <li class="active">Register</li>
        </ol>
    </div>
</div>
<!-- //breadcrumbs -->
<!-- register -->
<div class="register">
    <div class="container">
        <h3 class="animated wow zoomIn" data-wow-delay=".5s">Register Here</h3>
        <p class="est animated wow zoomIn" data-wow-delay=".5s">Welcome, please enter the following details to continue.
            If you have previously registered with us, <a href="/login">click here</a></p>
        <div id="alert"></div>
        <div class="login-form-grids">
            <form id="register" onsubmit="register()" class="animated wow slideInUp" data-wow-delay=".5s">
                <p style="padding-bottom: 15px"><input id="fullName" type="text" required="true" name="fullName" placeholder="Full Name..."/></p>

                <ul><input id="email" type="email" required="true" name="email" placeholder="Email..."/></ul>

                <ul><input id="password" type="password" required="true" name="pass_confirmation" data-validation="length" data-validation-length="min8" placeholder="Password..."/></ul>

                <ul> <input type="password" required="true" name="pass" data-validation="confirmation" placeholder="Password Confirmation..."></ul>

                <p style="padding-left: 50px"><br>
                    <input data-validation="recaptcha" data-validation-recaptcha-sitekey="[RECAPTCHA_SITEKEY]">
                </p>

                <p class="est animated wow zoomIn" data-wow-delay=".5s">By clicking this button, you are agree to our  <a href="/term">Policy Terms and Conditions.</a></p>

                <input type="submit" value="Register">
            </form>
        </div>
        <div class="register-home animated wow slideInUp" data-wow-delay=".5s">
            <a href="/">Home</a>
        </div>
    </div>
</div>

<div id="spinner" class="login text-center" hidden="hidden">
    <i class="fa fa-spinner fa-spin" style="font-size:30px"></i>
</div>

<div class="login" hidden="hidden">
    <div class="container">
        <h4 class="animated wow slideInUp" data-wow-delay=".5s">Thank you for registering with us.<span> We have already sent you an email to activate your account. Please check it!</span></h4>
        <p class="animated wow slideInUp" data-wow-delay=".5s"><a href="/login">Login Here</a> (Or) go back to <a href="/">Home<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a></p>
    </div>
</div>
<!-- //register -->
<!-- footer -->
<jsp:include page="footer.jsp"></jsp:include>
<!-- //footer -->

<script>
    $.validate({
        modules: 'location, date, security, file',
        form : '#register',
        reCaptchaSiteKey: '6LcYRA0UAAAAAE8M0z0qjO21bvvuYHXhTuRsPOwy',
        reCaptchaTheme: 'light',
        onModulesLoaded : function() {
            console.log('All modules loaded!');
        }
    });

    function register() {
        event.preventDefault();
        $(".register").hide();
        $("#spinner").show();
        var registerURL = "/register";
        var obj = $("#fullName").val() + "," + $("#email").val() + "," + $("#password").val();
        console.log("Test");
        $.ajax({
            type: 'POST',
            url: registerURL,
            contentType: 'application/json',
            data: obj,
            headers:
                {
                    acceptHeader: 'application/json'
                },
            traditional: true,
            timeout: 20000,
            success: function (response) {

                var alert = "<div class=\"alert alert-danger text-center\" role=\"alert\" id=\"alert\">" +
                    "<p>" + "<strong>" + "Email has already existed!" + "</strong>" + "</p>" + "</div>";
                console.log(response);
                if (response == "Error") {
                    $("#spinner").fadeOut();
                    $(".register").show();
                    $("#alert").empty();
                    $("#alert").append(alert);
                } else {
                    $("#spinner").fadeOut();
                    $(".login").show();
                }
            },
            error: function (e) {
                console.log("ERROR: ", e);
            }
        });
    }
</script>
</body>
</html>