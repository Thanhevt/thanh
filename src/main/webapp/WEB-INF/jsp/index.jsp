<!--
Author: Anh Thu and Ngoc Thanh

-->

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>Flowers for Dreams</title>
    <jsp:include page="head.jsp"></jsp:include>
</head>

<body>
<!-- header -->
<jsp:include page="header.jsp"></jsp:include>
<!-- //header -->
<!-- banner -->
<div class="banner">
    <!-- //banner -->
    <div class="container">
        <div class="banner-info animated wow zoomIn animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: zoomIn;">
            <h3>Flowers for Dreams</h3>
            <h4>Up to <span>50% <i>Off/-</i></span></h4>
            <div class="wmuSlider example1" style="overflow: hidden; height: 123px;">
                <div class="wmuSliderWrapper">
                    <article style="position: absolute; width: 100%; opacity: 0;">
                        <div class="banner-wrap">
                            <div class="banner-info1">
                                <p>Birthdays</p>
                            </div>
                        </div>
                    </article>
                    <article style="position: absolute; width: 100%; opacity: 0;">
                        <div class="banner-wrap">
                            <div class="banner-info1">
                                <p>Love & Romance</p>
                            </div>
                        </div>
                    </article>
                    <article style="position: relative; width: 100%; opacity: 1;">
                        <div class="banner-wrap">
                            <div class="banner-info1">
                                <p>Engagement + Wedding</p>
                            </div>
                        </div>
                    </article>
                </div>
                <ul class="wmuSliderPagination"><li><a href="#" class="">0</a></li><li><a href="#" class="">1</a></li><li><a href="#" class="wmuActive">2</a></li></ul></div>
            <script src="/resources/js/jquery.wmuSlider.js"></script>
            <script>
                $('.example1').wmuSlider();
            </script>
        </div>
    </div>
</div>
<!-- //banner-bottom -->
<!-- collections -->
<div class="new-collections">
    <div class="container">
        <h3 class="animated wow zoomIn" data-wow-delay=".5s">New & Beautiful Flowers</h3>
        <p class="est animated wow zoomIn" data-wow-delay=".5s">Our selection of new & beautiful flowers is always changing to reflect the most seasonal,
            stylish bouquets and gifts. So if you are looking for trendy just something truly unique, look no further.</p>

        <div class="new-collections-grids">
            <c:forEach var="product" items="${products}">
                <div class="col-md-4 new-collections-grid">
                    <div class="new-collections-grid1 animated wow slideInUp" data-wow-delay=".5s">
                        <div class="new-collections-grid1-image">
                            <a href="/detail?id=${product.id}" class="product-image"><img src="/resources/images/newflowers/${product.photoList[0].path}" alt=" " class="img-responsive" /></a>
                            <div class="new-collections-grid1-image-pos">
                                <a href="/detail?id=${product.id}">Quick View</a>
                            </div>
                            <div class="new-collections-grid1-right">
                                <div class="rating">
                                    <div class="rating-left">
                                        <img src="/resources/images/2.png" alt=" " class="img-responsive" />
                                    </div>
                                    <div class="rating-left">
                                        <img src="/resources/images/2.png" alt=" " class="img-responsive" />
                                    </div>
                                    <div class="rating-left">
                                        <img src="/resources/images/2.png" alt=" " class="img-responsive" />
                                    </div>
                                    <div class="rating-left">
                                        <img src="/resources/images/1.png" alt=" " class="img-responsive" />
                                    </div>
                                    <div class="rating-left">
                                        <img src="/resources/images/1.png" alt=" " class="img-responsive" />
                                    </div>
                                    <div class="clearfix"> </div>
                                </div>
                            </div>
                        </div>
                        <h4><a href="/detail?id=${product.id}">${product.name}</a></h4>
                        <div class="new-collections-grid1-left simpleCart_shelfItem">
                            <p><i>$${product.actualPrice}</i><span class="item_price">$${product.discountedPrice}</span><a class="item_add" href="#">add to cart </a></p>
                        </div>
                    </div>
                </div>
            </c:forEach>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!-- //collections -->
<!-- new-timer -->
<div class="timer">
    <div class="container">
        <div class="timer-grids">
            <div class="col-md-8 timer-grid-left animated wow slideInLeft" data-wow-delay=".5s">
                <h3><a href="products.jsp">100 Blooms of Floral-Fetti</a></h3>
                <div class="rating">
                    <div class="rating-left">
                        <img src="/resources/images/2.png" alt=" " class="img-responsive" />
                    </div>
                    <div class="rating-left">
                        <img src="/resources/images/2.png" alt=" " class="img-responsive" />
                    </div>
                    <div class="rating-left">
                        <img src="/resources/images/2.png" alt=" " class="img-responsive" />
                    </div>
                    <div class="rating-left">
                        <img src="/resources/images/2.png" alt=" " class="img-responsive" />
                    </div>
                    <div class="rating-left">
                        <img src="/resources/images/1.png" alt=" " class="img-responsive" />
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="new-collections-grid1-left simpleCart_shelfItem timer-grid-left-price">
                    <p><i>$580</i> <span class="item_price">$550</span></p>
                    <h4>Need to describe.</h4>
                    <p><a class="item_add timer_add" href="#">add to cart </a></p>
                </div>
                <div id="counter"> </div>
                <script src="/resources/js/jquery.countdown.js"></script>
                <script src="/resources/js/script.js"></script>
            </div>
            <div class="col-md-4 timer-grid-right animated wow slideInRight" data-wow-delay=".5s">
                <div class="timer-grid-right1">
                    <img src="/resources/images/newflowers/NF08-100 Blooms of Floral-Fetti.jpg" alt=" " class="img-responsive" />
                    <div class="timer-grid-right-pos">
                        <h4>Special Offer</h4>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!-- //new-timer -->
<!-- collections-bottom -->
<div class="collections-bottom">
    <div class="container">
        <div class="collections-bottom-grids">
            <div class="collections-bottom-grid animated wow slideInLeft" data-wow-delay=".5s">
                <h3>45% Offer For <span>Wedding & Engagement</span></h3>
            </div>
        </div>
        <div class="newsletter animated wow slideInUp" data-wow-delay=".5s">
            <h3>Newsletter</h3>
            <p>Join us now to get all news and special offers.</p>
            <form>
                <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                <input type="email" value="Enter your email address" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter your email address';}" required="">
                <input type="submit" value="Join Us" >
            </form>
        </div>
    </div>
</div>
<!-- //collections-bottom -->
<!-- footer -->
<jsp:include page="footer.jsp"></jsp:include>
<!-- //footer -->
</body>
</html>