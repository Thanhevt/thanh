<%--
  Created by IntelliJ IDEA.
  User: thu.phan
  Date: 10/26/17
  Time: 4:16 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Flowers for Dreams | Terms and Conditions</title>
    <jsp:include page="head.jsp"></jsp:include>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
<div class="breadcrumbs">
    <div class="container">
        <ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
            <li><a href="/"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
            <li class="active">Terms and Conditions</li>
        </ol>
    </div>
</div>
<div class="container" style="padding-bottom:50px">
<div class="login">
    <div class="container">
        <h3 class="animated wow zoomIn" data-wow-delay=".5s">Terms and Conditions</h3>
    </div>
</div>
<div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 animated wow zoomIn" data-wow-delay=".5s" style="text-align:left">
    <p>Welcome to our online store! Dreams Flowers Shop and its associates provide their services to you subject to the following conditions. If you visit or shop within this website, you accept these conditions. Please read them carefully. ​</p><br>
    <p>Please review our Privacy Notice, which also governs your visit to our website, to understand our practices.</p><br>
    <p>When you visit Dreams Flowers Shop or send e-mails to us, you are communicating with us electronically. You consent to receive communications from us electronically. We will communicate with you by e-mail or by posting notices on this site. You agree that all agreements, notices, disclosures and other communications that we provide to you electronically satisfy any legal requirement that such communications be in writing.
    </p><br>
    <p>All content included on this site, such as text, graphics, logos, button icons, images, audio clips, digital downloads, data compilations, and software, is the property of MYCOMPANY or its content suppliers and protected by international copyright laws. The compilation of all content on this site is the exclusive property of Dreams Flowers Shop, with copyright authorship for this collection Dreams Flowers Shop , and protected by international copyright laws.</p><br>
    <p>If you use this site, you are responsible for maintaining the confidentiality of your account and password and for restricting access to your computer, and you agree to accept responsibility for all activities that occur under your account or password. If you are under 18, you may use our website only with involvement of a parent or guardian. Dreams Flowers Shop and its associates reserve the right to refuse service, terminate accounts, remove or edit content, or cancel orders in their sole discretion.
    </p><br>
</div>
</div>
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
