<%--
  Created by IntelliJ IDEA.
  User: thu.phan
  Date: 10/16/17
  Time: 8:40 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<title>Flowers for Dreams</title>
<jsp:include page="head.jsp"></jsp:include>
</head>
<body>
<div class="header">
    <div class="container">
        <div class="header-grid">
            <div class="header-grid-left animated wow slideInLeft" data-wow-delay=".5s">
                <ul>
                    <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="mailto:info@example.com">@example.com</a></li>
                    <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>+1 773 886 9848</li>
                    <c:if test="${username == null}">
                        <li><i class="glyphicon glyphicon-log-in" aria-hidden="true"></i><a href="/login">Login</a></li>
                        <li><i class="glyphicon glyphicon-book" aria-hidden="true"></i><a href="/register">Register</a></li>
                    </c:if>
                    <c:if test="${username != null}">
                        <li><i class="glyphicon glyphicon-user" aria-hidden="true"></i>${username}</li>
                        <li><i class ="glyphicon glyphicon-log-out" aria-hidden="true"></i><a href="/logout">Logout</a></li>
                    </c:if>
                </ul>
            </div>
            <div class="header-grid-right animated wow slideInRight" data-wow-delay=".5s">
                <ul class="social-icons">
                    <li><a href="#" class="facebook"></a></li>
                    <li><a href="#" class="twitter"></a></li>
                    <li><a href="#" class="g"></a></li>
                    <li><a href="#" class="instagram"></a></li>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="logo-nav">
            <div class="logo-nav-left animated wow zoomIn" data-wow-delay=".5s">
                <h1><a href="/">DREAMS<span>Flowers Shop</span></a></h1>
            </div>
            <div class="logo-nav-left1">
                <nav class="navbar navbar-default">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header nav_2">
                        <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="/" class="act">Home</a></li>
                            <!-- Mega Menu -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">${category1[0].cateName}<b class="caret"></b></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <c:forEach var="cate" items="${category1}">
                                        <ul class="multi-column-dropdown">
                                            <li><a href="/category?id=${cate.id}">${cate.name}</a></li>
                                        </ul>
                                    </c:forEach>
                                    <div class="clearfix"></div>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">${category2[0].cateName}<b class="caret"></b></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <c:forEach var="cate" items="${category2}">
                                        <ul class="multi-column-dropdown">
                                            <li><a href="/category?id=${cate.id}">${cate.name}</a></li>
                                        </ul>
                                    </c:forEach>
                                    <div class="clearfix"></div>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">${category3[0].cateName}<b class="caret"></b></a>
                                <ul class="dropdown-menu multi-column columns-3">
                                    <c:forEach var="cate" items="${category3}">
                                        <ul class="multi-column-dropdown">
                                            <li><a href="/category?id=${cate.id}">${cate.name}</a></li>
                                        </ul>
                                    </c:forEach>
                                    <div class="clearfix"></div>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="logo-nav-right">
                <div class="search-box">
                    <div id="sb-search" class="sb-search">
                        <form>
                            <input class="sb-search-input" placeholder="Enter your search term..." type="search" id="search">
                            <input class="sb-search-submit" type="submit" value="">
                            <span class="sb-icon-search"> </span>
                        </form>
                    </div>
                </div>
                <!-- search-scripts -->
                <script src="/resources/js/classie.js"></script>
                <script src="/resources/js/uisearch.js"></script>
                <script>
                    new UISearch( document.getElementById( 'sb-search' ) );
                </script>
                <!-- //search-scripts -->
            </div>
            <div class="header-right">
                <div class="cart box_1">
                    <a href="/checkout">
                        <h3> <div class="total">
                            <span class="simpleCart_total"></span> (<span id="simpleCart_quantity" class="simpleCart_quantity"></span> items)</div>
                            <img src="/resources/images/bag.png" alt="" />
                        </h3>
                    </a>
                    <p><a href="javascript:;" class="simpleCart_empty">Empty Cart</a></p>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
</body>
</html>
