<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<html lang="en">
    <head>
    <title>Flowers for Dreams | Checkout</title>
    <jsp:include page="head.jsp"></jsp:include>
    </head>
    <body>
        <div class="header">
            <div class="container">
                <div class="header-top">
                    <div class="logo">
                        <a href="index.html">N-AIR</a>
                    </div>
                    <div class="login-bars">
                        <a class="btn btn-default log-bar" href="register.jsp" role="button">Sign up</a>
                        <a class="btn btn-default log-bar" href="signup.jsp" role="button">Login</a>
                        <div class="cart box_1">
                            <a href="checkout.jsp">
                            <h3>
                                <div class="total">
<span class="simpleCart_total"></span>(<span id="simpleCart_quantity" class="simpleCart_quantity"></span>)</div></h3>
                            </a>
                            <p><a href="javascript:;" class="simpleCart_empty">Empty Cart</a></p>
                            <div class="clearfix"> </div>
                        </div>	
                    </div>
        <div class="clearfix"></div>
                </div>
                <!---menu-----bar--->
                <div class="header-botom">
                    <div class="content white">
                    <nav class="navbar navbar-default nav-menu" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <!--/.navbar-header-->

                        <div class="collapse navbar-collapse collapse-pdng" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav nav-font">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Shop<b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="products.jsp">Shoes</a></li>
                                        <li><a href="products.jsp">Tees</a></li>
                                        <li><a href="products.jsp">Tops</a></li>
                                        <li class="divider"></li>
                                        <li><a href="products.jsp">Tracks</a></li>
                                        <li class="divider"></li>
                                        <li><a href="products.jsp">Gear</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Men<b class="caret"></b></a>
                                    <ul class="dropdown-menu multi-column columns-3">
                                        <div class="row">
                                            <div class="col-sm-4 menu-img-pad">
                                                <ul class="multi-column-dropdown">
                                                    <li><a href="products.jsp">Joggers</a></li>
                                                    <li><a href="products.jsp">Foot Ball</a></li>
                                                    <li><a href="products.jsp">Cricket</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="products.jsp">Tennis</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="products.jsp">Casual</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-4 menu-img-pad">
                        <a href="#"><img src="/resources/images/menu1.jpg" alt="/" class="img-rsponsive men-img-wid" /></a>
                                            </div>
                                            <div class="col-sm-4 menu-img-pad">
                        <a href="#"><img src="/resources/images/menu2.jpg" alt="/" class="img-rsponsive men-img-wid" /></a>
                                            </div>
                                        </div>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Women<b class="caret"></b></a>
                                    <ul class="dropdown-menu multi-column columns-2">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <ul class="multi-column-dropdown">
                                                    <li><a href="products.jsp">Tops</a></li>
                                                    <li><a href="products.jsp">Bottoms</a></li>
                                                    <li><a href="products.jsp">Yoga Pants</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="products.jsp">Sports</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="products.jsp">Gym</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6">
                        <a href="#"><img src="/resources/images/menu3.jpg" alt="/" class="img-rsponsive men-img-wid" /></a>
                                            </div>
                                        </div>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">kids<b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="products.jsp">Tees</a></li>
                                        <li><a href="products.jsp">Shorts</a></li>
                                        <li><a href="products.jsp">Gear</a></li>
                                        <li class="divider"></li>
                                        <li><a href="products.jsp">Watches</a></li>
                                        <li class="divider"></li>
                                        <li><a href="products.jsp">Shoes</a></li>
                                    </ul>
                                </li>
                                <li><a href="contact.html">Catch</a></li>
                                <div class="clearfix"></div>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <!--/.navbar-collapse-->
                        <div class="clearfix"></div>
                    </nav>
                    <!--/.navbar-->   
                        <div class="clearfix"></div>
                    </div>
                    <!--/.content--->
                </div>
                    <!--header-bottom-->
            </div>
        </div>
        <div class="head-bread">
            <div class="container">
                <ol class="breadcrumb">
                    <li><a href="index.html">Home</a></li>
                    <li><a href="products.hml">Products</a></li>
                    <li class="active">CART</li>
                </ol>
            </div>
        </div>
        <!-- check-out -->
            <div class="check">
                <div class="container">	 
                    <div class="col-md-3 cart-total">
                        <a class="continue" href="#">Continue to basket</a>
                        <div class="price-details">
                            <h3>Price Details</h3>
                            <span>Total</span>
                            <span class="total1">${sum}</span>
                            <span>Discount</span>
                            <span class="total1">10%(Festival Offer)</span>
                            <%--<span>Delivery Charges</span>--%>
                            <%--<span class="total1">150.00</span>--%>
                            <div class="clearfix"></div>				 
                        </div>
                        <hr class="featurette-divider">
                        <ul class="total_price">
                           <li class="last_price"> <h4>TOTAL</h4></li>	
                           <li class="last_price"><span>${sum}</span></li>
                           <div class="clearfix"> </div>
                        </ul> 
                        <div class="clearfix"></div>
                        <a class="order" href="/cart/payment">Place Order</a>
                    </div>
                    <div class="col-md-9 cart-items">
                        <h1>My Shopping Bag (${cartItemList.size()  })</h1>
                            <%--<script>$(document).ready(function(c) {--%>
                                <%--$('.close1').on('click', function(c){--%>
                                    <%--$('.cart-header').fadeOut('slow', function(c){--%>
                                        <%--$('.cart-header').remove();--%>
                                    <%--});--%>
                                    <%--});	  --%>
                                <%--});--%>
                           <%--</script>--%>
                        <c:forEach items="${cartItemList}" var="cartItem">
                            <div class="cart-header">
                                <div class="close"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></div>
                                <div class="cart-sec simpleCart_shelfItem">
                                        <div class="cart-item cyc">
                                            <img src="/resources/images/${cartItem.photoList[0].path}" class="img-responsive" alt=""/>
                                        </div>
                                       <div class="cart-item-info">
                                            <ul class="qty">
                                                <li><p>Size : ${cartItem.size} US</p></li>
                                                <li><p>Qty : ${cartItem.quantity}</p></li>
                                                <li><p>Price each : $${cartItem.price}</p></li>
                                            </ul>
                                            <div class="delivery">
                                                 <p>Total : $${cartItem.total}</p>
                                                 <span>Delivered in 2-3 bussiness days</span>
                                                 <div class="clearfix"></div>
                                            </div>
                                       </div>
                                       <div class="clearfix"></div>

                                  </div>
                             </div>
                        </c:forEach>
                         <%--<script>$(document).ready(function(c) {--%>
                                <%--$('.close2').on('click', function(c){--%>
                                        <%--$('.cart-header2').fadeOut('slow', function(c){--%>
                                    <%--$('.cart-header2').remove();--%>
                                <%--});--%>
                                <%--});	  --%>
                                <%--});--%>
                         <%--</script>--%>
                        <%--<div class="cart-header2">--%>
                <%--<div class="close2"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></div>--%>
                                <%--<div class="cart-sec simpleCart_shelfItem">--%>
                                    <%--<div class="cart-item cyc">--%>
                                         <%--<img src="/resources/images/grid7.jpg" class="img-responsive" alt=""/>--%>
                                    <%--</div>--%>
                                    <%--<div class="cart-item-info">--%>
                                        <%--<ul class="qty">--%>
                                            <%--<li><p>Size : 8 US</p></li>--%>
                                            <%--<li><p>Qty : 2</p></li>--%>
                                            <%--<li><p>Price each : $190</p></li>--%>
                                        <%--</ul>--%>
                                        <%--<div class="delivery">--%>
                                            <%--<p>Service Charges : Rs.360.00</p>--%>
                                            <%--<span>Delivered in 2-3 bussiness days</span>--%>
                                            <%--<div class="clearfix"></div>--%>
                                        <%--</div>	--%>
                                   <%--</div>--%>
                                   <%--<div class="clearfix"></div>					--%>
                                <%--</div>--%>
                        <%--</div>			--%>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <div class="footer-grid">
            <div class="container">
                <div class="col-md-2 re-ft-grd">
                    <h3>Categories</h3>
                    <ul class="categories">
                        <li><a href="#">Men</a></li>
                        <li><a href="#">Women</a></li>
                        <li><a href="#">Kids</a></li>
                        <li><a href="#">Formal</a></li>
                        <li><a href="#">Casuals</a></li>
                        <li><a href="#">Sports</a></li>
                    </ul>
                </div>
                <div class="col-md-2 re-ft-grd">
                    <h3>Short links</h3>
                    <ul class="shot-links">
                        <li><a href="#">Contact us</a></li>
                        <li><a href="#">Support</a></li>
                        <li><a href="#">Delivery</a></li>
                        <li><a href="#">Return Policy</a></li>
                        <li><a href="#">Terms & conditions</a></li>
                        <li><a href="contact.html">Sitemap</a></li>
                    </ul>
                </div>
                <div class="col-md-6 re-ft-grd">
                    <h3>Social</h3>
                    <ul class="social">
                        <li><a href="#" class="fb">facebook</a></li>
                        <li><a href="#" class="twt">twitter</a></li>
                        <li><a href="#" class="gpls">g+ plus</a></li>
                        <div class="clearfix"></div>
                    </ul>
                </div>
                <div class="col-md-2 re-ft-grd">
                    <div class="bt-logo">
                        <div class="logo">
                            <a href="index.html" class="ft-log">N-AIR</a>
                        </div>
                    </div>
                </div>
        <div class="clearfix"></div>
            </div>
            <div class="copy-rt">
                <div class="container">
            <p>&copy;   2015 N-AIR All Rights Reserved. Design by <a href="http://www.w3layouts.com">w3layouts</a></p>
                </div>
            </div>
        </div>
    </body>
</html>