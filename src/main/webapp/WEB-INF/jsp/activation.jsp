<%--
Created by IntelliJ IDEA.
User: thu.phan
Date: 10/11/17
Time: 11:01 AM
To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<title>Flowers for Dreams | Activation</title>
<jsp:include page="head.jsp"></jsp:include>
</head>
<body>
<!-- header -->
<jsp:include page="header.jsp"></jsp:include>
<div class="breadcrumbs">
    <div class="container">
        <ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
            <li><a href="/"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
            <li class="active">Activation</li>
        </ol>
    </div>
</div>
<!--end of header-->
<div class ="login">
    <div class = "container">
        <c:if test="${msg != null}">
            <h4 class="animated wow slideInUp" data-wow-delay=".5s">${msg}</h4>
            <p class="animated wow slideInUp" data-wow-delay=".5s">Click<a href="/login"> here </a>to login!</p>
        </c:if>

        <c:if test="${error != null}">
            <h4 class="animated wow slideInUp" data-wow-delay=".5s">${error}</h4>
            <p class="animated wow slideInUp" data-wow-delay=".5s"><a href="/register">Register Here</a> (Or) go back to <a href="/">Home<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a></p>
        </c:if>
    </div>
</div>
<!-- footer -->
<jsp:include page="footer.jsp"></jsp:include>
<!-- //footer -->
</body>
</html>
