package online.flowers.repository;

import online.flowers.entity.CategoryEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CategoryRepository extends CrudRepository<CategoryEntity, Integer> {
    CategoryEntity findById (int id);
    List<CategoryEntity> findAll();
    List<CategoryEntity> findByCateName (String cateName);
}
