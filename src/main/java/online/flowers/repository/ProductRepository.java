package online.flowers.repository;

import online.flowers.entity.ProductEntity;
import online.flowers.entity.UserEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository

public interface ProductRepository extends CrudRepository<ProductEntity, Integer> {

    ProductEntity findById(int id);
    List<ProductEntity> findAll();
    List<ProductEntity> findByCategoryId (int id);
}
