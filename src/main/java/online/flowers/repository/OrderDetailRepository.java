package online.flowers.repository;

import online.flowers.entity.OrderDetailEntity;
import org.springframework.data.repository.CrudRepository;

public interface OrderDetailRepository
    extends CrudRepository<OrderDetailEntity, Integer>{
}
