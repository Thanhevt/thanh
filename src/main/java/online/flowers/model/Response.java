package online.flowers.model;

public class Response {
    private String status;

    public Response(){

    }

    public Response(String status, Object data) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
