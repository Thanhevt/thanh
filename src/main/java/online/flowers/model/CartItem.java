package online.flowers.model;

import online.flowers.entity.ProductEntity;

public class CartItem extends ProductEntity{
    private ProductEntity product;
    private int quantity;
    private float total;

    public CartItem(ProductEntity product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public ProductEntity getProduct() {
        return product;
    }

    public void setProduct(ProductEntity product) {
        this.product = product;
    }
}