package online.flowers.controller;


//import com.sun.tools.internal.xjc.reader.xmlschema.bindinfo.BIConversion;
import online.flowers.entity.CategoryEntity;
import online.flowers.entity.CommentEntity;
import online.flowers.entity.ProductEntity;
import online.flowers.entity.UserEntity;
import online.flowers.repository.CategoryRepository;
import online.flowers.repository.CommentRepository;
import online.flowers.repository.ProductRepository;
import online.flowers.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/")

public class HomeController {

    private static final int dataPerPage = 4;

    @Autowired
    private CategoryRepository categoryRepository;


    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CommentRepository commentRepository;

    @RequestMapping()
    public String showProducts(Model model) {
        showCategory(model);
        List<ProductEntity> products = productRepository.findByCategoryId(7);
        model.addAttribute("products", products);
        return "index";

    }

    @RequestMapping(value = "/detail")
    public String showDetail(Model model, @RequestParam("id") String id, HttpSession session) {
        showCategory(model);
        ProductEntity productEntity = productRepository.findById(Integer.valueOf(id));
        if (session.getAttribute("email") != null) {
            UserEntity user = userRepository.findByEmail(session.getAttribute("email").toString());
            model.addAttribute("product", productEntity);
            model.addAttribute("username", user.getFullName());
            model.addAttribute("userId", user.getId());
            return "single";
        } else {
            model.addAttribute("product", productEntity);
            return "single";
        }
    }

    @RequestMapping(value = "/category")
    public String showCategory(Model model, @RequestParam("id") String id) {
        List<ProductEntity> productList = productRepository.findByCategoryId(Integer.valueOf(id));
        model.addAttribute("productList", productList);
        showNewFlowers(model);
        return "products";

    }

    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    @ResponseBody
    public String addComment(Model model, @RequestBody String comment, HttpSession session) {
        String[] commentArr = comment.split(",");
        CommentEntity newComment = new CommentEntity();
        UserEntity user = new UserEntity();
        if (userRepository.findById(Integer.valueOf(commentArr[0])) != null) {
            // save comment to DB
            user = userRepository.findById(Integer.valueOf(commentArr[0]));
            newComment.setUser(user);
            newComment.setContent(commentArr[2]);
            Date d1 = new Date(System.currentTimeMillis());
            SimpleDateFormat df = new SimpleDateFormat("MM/dd/YYYY HH:mm a");
            String formattedDate = df.format(d1);
            newComment.setCreatedAt(d1);
            newComment.setParentId(null);
            commentRepository.save(newComment);
        }
            return user.getFullName() + "||" + newComment.getContent() + "||" + newComment.getCreatedAt();
    }

    @RequestMapping(value = "/category/page")
    @ResponseBody
    public String viewCategoryPaging(
            @RequestParam("id") String id,
            @RequestParam("page") String page,
            Model model
    ) {
        int categoryId = Integer.parseInt(id);
        List<ProductEntity> productList =
                productRepository.findByCategoryId(categoryId);
        List<ProductEntity> result =
                getProductViaPaging(productList, Integer.parseInt(page));
        String data = "";
        for (ProductEntity product : result) {
            data += "<div class=\"col-md-4 new-collections-grid\">"+
                    "                            <div class=\"new-collections-grid1 animated wow slideInUp\" data-wow-delay=\".5s\">"+
                    "                                <div class=\"new-collections-grid1-image\">"+
                    "                                    <a href=\"/detail?id=" + product.getId() + "class=\"product-image\"><img src=\"/resources/images/newflowers/"+ product.getPhotoList().get(0).getPath()+" alt=\" \" class=\"img-responsive\" /></a>"+
                    "                                    <div class=\"new-collections-grid1-image-pos\">"+
                    "                                        <a href=\"/detail?id=" + product.getId() +">Quick View</a>"+
                    "                                    </div>"+
                    "                                    <div class=\"new-collections-grid1-right\">"+
                    "                                        <div class=\"rating\">"+
                    "                                            <div class=\"rating-left\">"+
                    "                                                <img src=\"/resources/images/2.png\" alt=\" \" class=\"img-responsive\" />"+
                    "                                            </div>"+
                    "                                            <div class=\"rating-left\">"+
                    "                                                <img src=\"/resources/images/2.png\" alt=\" \" class=\"img-responsive\" />"+
                    "                                            </div>"+
                    "                                            <div class=\"rating-left\">"+
                    "                                                <img src=\"/resources/images/2.png\" alt=\" \" class=\"img-responsive\" />"+
                    "                                            </div>"+
                    "                                            <div class=\"rating-left\">"+
                    "                                                <img src=\"/resources/images/1.png\" alt=\" \" class=\"img-responsive\" />"+
                    "                                            </div>"+
                    "                                            <div class=\"rating-left\">"+
                    "                                                <img src=\"/resources/images/1.png\" alt=\" \" class=\"img-responsive\" />"+
                    "                                            </div>"+
                    "                                            <div class=\"clearfix\"> </div>"+
                    "                                        </div>"+
                    "                                    </div>"+
                    "                                </div>"+
                    "                                <h4><a href=\"/detail?id="+ product.getId() + "\">"+ product.getName()+"</a></h4>"+
                    "                                <div class=\"new-collections-grid1-left simpleCart_shelfItem\">"+
//                    "                                    <p><span class=\"item_price\">$" + product.getPrice() +"</span><a class=\"item_add\" href=\"#\">add to cart </a></p>"+
                    "                                </div>"+
                    "                            </div>"+
                    "                        </div>";


        }

            return data;
    }


        private List<ProductEntity> getProductViaPaging (
                List <ProductEntity> productList,
        int page){
            List<ProductEntity> result = new ArrayList<>();
            int start = (page - 1) * dataPerPage;
            for (int i = start; i < start + dataPerPage; i++) {
                result.add(productList.get(i));
            }
            return result;
        }

    public void showNewFlowers(Model model) {
        List<ProductEntity> newProducts = productRepository.findByCategoryId(7);
        model.addAttribute("newProducts", newProducts);
    }

    public void showCategory(Model model) {
        List<CategoryEntity> categoryList1 = categoryRepository.findByCateName("BIRTHDAY");
        List<CategoryEntity> categoryList2 = categoryRepository.findByCateName("OCCASION");
        List<CategoryEntity> categoryList3 = categoryRepository.findByCateName("FLOWERS");

        model.addAttribute("category1", categoryList1);
        model.addAttribute("category2", categoryList2);
        model.addAttribute("category3", categoryList3);
    }



}


