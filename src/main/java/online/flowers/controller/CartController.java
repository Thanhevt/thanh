//package online.flowers.controller;
//
//
//import online.flowers.entity.OrderDetailEntity;
//import online.flowers.entity.OrderEntity;
//import online.flowers.entity.ProductEntity;
//import online.flowers.model.CartItem;
//import online.flowers.repository.OrderDetailRepository;
//import online.flowers.repository.OrderRepository;
//import online.flowers.repository.impl.ProductRepositoryImpl;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Scope;
//import org.springframework.context.annotation.ScopedProxyMode;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.ui.ModelMap;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpSession;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//@Controller
//@RequestMapping(value = "/cart")
//@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
//public class CartController {
//    @Autowired
//    ProductRepositoryImpl productRepository;
//
//    @Autowired
//    OrderRepository orderRepository;
//
//    @Autowired
//    OrderDetailRepository orderDetailRepository;
//
//    // /cart?id=1
//    @RequestMapping(value = "add")
//    public String add(
//            @RequestParam(name = "id") String id,
//            HttpServletRequest request) {
//        HttpSession session = request.getSession();
//        Object myCart = session.getAttribute("myCart");
//        int productId = Integer.parseInt(id);
//        int limitedAddtoCart = 0;
//
//        ProductEntity productEntity = productRepository.findById(productId);
//        // Found a product via product id -> go ahead
//        if (productEntity != null) {
//            if (myCart == null) {
//                List<CartItem> cartItemList = new ArrayList<CartItem>();
//                cartItemList.add(new CartItem(productEntity, 1));
//                session.setAttribute("myCart", cartItemList);
//            } else {
//                List<CartItem> cartItemList = (List<CartItem>) myCart;
//                int index = isExists(productId, session);
//                if (index == -1) {
//                    cartItemList.add(new CartItem(productEntity, 1));
//                } else {
//                    int quantity = cartItemList.get(index).getQuantity() + 1;
//                    if (quantity < 4) {
//                        cartItemList.get(index).setQuantity(quantity);
//                    } else {
//                        limitedAddtoCart = 1;
//                    }
//                    session.setAttribute("myCart", cartItemList);
//                    request.setAttribute("limitedAddtoCart", limitedAddtoCart);
//                }
//                session.setAttribute("myCart", cartItemList);
//            }
//            return "redirect:/detail?id=" + id;
//        }
//        // end-user provided a wrong product IT
//        return "/";
//    }
//
//    @RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
//    public String delete(
//            @PathVariable("id") int id,
//            HttpSession session, ModelMap modelMap) {
//        List<CartItem> cartItemList = (List<CartItem>)
//                session.getAttribute("myCart");
//        int index = isExists(id, session);
//        cartItemList.remove(index);
//        session.setAttribute("myCart", cartItemList);
//        return "redirect:/cart/view";
//    }
//
//    @RequestMapping(value = "/update", method = RequestMethod.POST)
//    public String update(HttpServletRequest request, HttpSession session) {
//        List<CartItem> cartItemList = (List<CartItem>)
//                session.getAttribute("myCart");
//        String quantity = request.getParameter("quantity");
////        for (int i=0; i<cartItemList.size();i++){
////            cartItemList.get(0).setQuantity(Integer.parseInt(quantity));
////        }
//        String id = request.getParameter("id");
//        int index = isExists(Integer.parseInt(id), session);
//        cartItemList.get(index).setQuantity(Integer.parseInt(quantity));
//        session.setAttribute("myCart", cartItemList);
//        return "redirect:/cart/view";
//    }
//
//    @RequestMapping(value = "view")
//    public String showCart(HttpServletRequest request,
//                           Model model) {
//        HttpSession session = request.getSession();
//        Object myCart = session.getAttribute("myCart");
//        if (myCart != null) {
//            List<CartItem> cartItemList = (List<CartItem>) myCart;
//            model.addAttribute("cartItemList", cartItemList);
//            float sum = 0;
//            for (CartItem item : cartItemList) {
//                sum += item.getTotal();
//            }
//            session.setAttribute("cart-total", sum);
//            model.addAttribute("sum", sum);
//        }
//        return "checkout";
//    }
//
//    @RequestMapping(value = "payment")
//    public String checkOut(Model model) {
//        model.addAttribute("order", new OrderEntity());
//        return "payment";
//    }
//
//    @RequestMapping(value = "payment", method = RequestMethod.POST)
//    public String payment(OrderEntity order,
//                          HttpServletRequest request) {
//        HttpSession session = request.getSession();
//        List<CartItem> cartItemList =
//                (List<CartItem>) session.getAttribute("myCart");
//
//        order.setOrderAt(new Date());
//        order.setTotal((Float) session.getAttribute("cart-total"));
//        List<OrderDetailEntity> orderDetailEntities = new ArrayList<>();
//        for (CartItem item : cartItemList) {
//            OrderDetailEntity orderDetailEntity = new OrderDetailEntity();
//            ProductEntity product = new ProductEntity();
//            product.setId(item.getId());
//            orderDetailEntity.setProduct(product);
//            orderDetailEntity.setQuantity(item.getQuantity());
//            orderDetailEntity.setTotal(item.getTotal());
//            orderDetailEntity.setOrder(order);
//            orderDetailEntities.add(orderDetailEntity);
//        }
//        order.setOrderDetailList(orderDetailEntities);
//
//        orderRepository.save(order);
//        orderDetailRepository.save(orderDetailEntities);
//        return "redirect:/";
//    }
//
//    private int isExists(int id, HttpSession session) {
//        List<CartItem> cartItemList = (List<CartItem>) session.getAttribute("myCart");
//        for (int i = 0; i < cartItemList.size(); i++)
//            if (cartItemList.get(i).getProduct().getId() == id)
//                return i;
//        return -1;
//    }
//}
