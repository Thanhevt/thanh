package online.flowers.controller;

import online.flowers.entity.CategoryEntity;
import online.flowers.entity.RoleEntity;
import online.flowers.entity.UserEntity;
import online.flowers.model.Response;
import online.flowers.repository.CategoryRepository;
import online.flowers.repository.UserRepository;
import online.flowers.service.SendConfirmationEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;


@Controller
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private CategoryRepository categoryRepository;

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String showRegister(Model model){
        showCategory(model);
        model.addAttribute("user", new UserEntity());
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ResponseBody
    public String submitRegister(Model model, @RequestBody String user){
        showCategory(model);
        String[] userArr = user.split(",");
        UserEntity userEntity = new UserEntity();
        userEntity.setFullName(userArr[0]);
        System.out.println(userEntity.getFullName());
        userEntity.setEmail(userArr[1]);
        userEntity.setPassword(userArr[2]);
        Response response = new Response();
        if (userRepository.findByEmail(userEntity.getEmail()) != null){
            // email has existed
            response.setStatus("Error");
            String error = response.getStatus();
            return error;
        }
        // email not existed
        RoleEntity role = new RoleEntity();
        role.setId(1);
        userEntity.setRole(role);
        userEntity.setIsActive(1);
        String hashPassword= passwordEncoder.encode(userEntity.getPassword());
        String activationCode = passwordEncoder.encode(userEntity.getEmail());
        SendConfirmationEmail sendConfirmationEmail = new SendConfirmationEmail();
        sendConfirmationEmail.sendEmail(activationCode, userEntity.getEmail());
        userEntity.setPassword(hashPassword);
        userEntity.setActivationCode(activationCode);
        userRepository.save(userEntity);
        response.setStatus("Success");
        String success = response.getStatus();
        return success;
    }

    @RequestMapping(value = "/activation", method = RequestMethod.GET)
    public String activateAccount (Model model, @RequestParam("code") String activationCode) {
        showCategory(model);
        String message = null;
        UserEntity user = userRepository.findByActivationCode(activationCode);
        if (user != null) {
            user.setActivationCode(null);
            userRepository.save(user);
            message = "Email has been verified successfully. Account has been activated.";
            model.addAttribute("msg", message);
        } else {
            message = "Wrong email validation input!";
            model.addAttribute("error", message );
        }

        return "activation";
    }

    @RequestMapping(value = "/term", method = RequestMethod.GET)
    public String showTermAndCondition(Model model) {
        showCategory(model);
        return "terms";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String showLogin(Model model){
        showCategory(model);
        model.addAttribute("user", new UserEntity());
        model.addAttribute("email", null);
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public String submitLogin(Model model,
                             @RequestBody String user,
                              HttpServletRequest request) {
        showCategory(model);
        String[] userArr = user.split(",");
        UserEntity userEntity = userRepository.findByEmail(userArr[0]);
        Response response = new Response();

        if (userEntity == null) {
            response.setStatus("Error");
            String error = response.getStatus();
            return error;
        } else if (userEntity.getActivationCode() == null) {
            String hashPassword = userEntity.getPassword();
            if (passwordEncoder.matches(userArr[1], hashPassword)) {
                HttpSession session = request.getSession();
                session.setAttribute("username", userEntity.getFullName());
                session.setAttribute("email", userEntity.getEmail());
                response.setStatus("Success");
                String error = response.getStatus();
                return error;
            } else {
                response.setStatus("Error");
                String error = response.getStatus();
                return error;
            }
        } else {
            response.setStatus("Activation");
            String activation = response.getStatus();
            return activation;
        }
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest request){
        request.getSession().invalidate();
        return "redirect:/";
    }


    public void showCategory(Model model) {
        List<CategoryEntity> categoryList1 = categoryRepository.findByCateName("BIRTHDAY");
        List<CategoryEntity> categoryList2 = categoryRepository.findByCateName("OCCASION");
        List<CategoryEntity> categoryList3 = categoryRepository.findByCateName("FLOWERS");

        model.addAttribute("category1", categoryList1);
        model.addAttribute("category2", categoryList2);
        model.addAttribute("category3", categoryList3);
    }


}
