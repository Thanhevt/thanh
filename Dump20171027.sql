-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: final_project
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cateName` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `promotionId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1yorwrwyfi9am0cc7n6hy97ky` (`promotionId`),
  CONSTRAINT `FK1yorwrwyfi9am0cc7n6hy97ky` FOREIGN KEY (`promotionId`) REFERENCES `product` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'BIRTHDAY','BIRTHDAY FOR MOM',NULL),(2,'BIRTHDAY','BIRTHDAY FOR WIFE',NULL),(3,'BIRTHDAY','BIRTHDAY FOR FRIEND',NULL),(4,'OCCASION','WEDDING',NULL),(5,'OCCASION','ANNIVERSARY',NULL),(6,'OCCASION','CONGRATULATIONS',NULL),(7,'FLOWERS','NEW FLOWERS',NULL),(8,'FLOWERS','BEST-SELLERS',NULL),(9,'FLOWERS','SEASONAL',NULL);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `parentId` varchar(255) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKkoanjyovpsls4byyo8b7jktri` (`userId`),
  CONSTRAINT `FKkoanjyovpsls4byyo8b7jktri` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (16,'sfgsgsgs','2017-10-27 16:14:54',NULL,5),(17,'sfaafagsg','2017-10-27 16:15:24',NULL,5),(18,'sfaafagsg','2017-10-27 16:26:59',NULL,5);
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orderDetail`
--

DROP TABLE IF EXISTS `orderDetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orderDetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` int(11) DEFAULT NULL,
  `total` float DEFAULT NULL,
  `orderId` int(11) DEFAULT NULL,
  `productId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKb51qdl923yoolek2tjtyati00` (`orderId`),
  KEY `FKakgcl1fas777r82docsj5ec5i` (`productId`),
  CONSTRAINT `FKakgcl1fas777r82docsj5ec5i` FOREIGN KEY (`productId`) REFERENCES `product` (`id`),
  CONSTRAINT `FKb51qdl923yoolek2tjtyati00` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orderDetail`
--

LOCK TABLES `orderDetail` WRITE;
/*!40000 ALTER TABLE `orderDetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `orderDetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fullName` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `orderAt` datetime DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `shippingTime` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `total` float DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4xqhrpwrfhpv4bif865ircrbj` (`userId`),
  CONSTRAINT `FK4xqhrpwrfhpv4bif865ircrbj` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photo`
--

DROP TABLE IF EXISTS `photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) DEFAULT NULL,
  `productId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKcp4ff8e72x4tbey0m8l98phwi` (`productId`),
  CONSTRAINT `FKcp4ff8e72x4tbey0m8l98phwi` FOREIGN KEY (`productId`) REFERENCES `product` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photo`
--

LOCK TABLES `photo` WRITE;
/*!40000 ALTER TABLE `photo` DISABLE KEYS */;
INSERT INTO `photo` VALUES (1,'FFM01-100 Birthday Blooms.jpg',1),(2,'FFM02-Deluxe Pink Sapphire Birthday Bouquet.jpg',2),(3,'FFM03-20 Multi-Colored Birthday Tulips.jpg',3),(4,'FFM04-Deluxe Birthday Smiles.jpg',4),(5,'FFM06-Birthday Sunflower Radiance.jpg',5),(6,'FFM07-100 Blooms of Sunshine.jpg',6),(7,'FFM08-YAY Blooming Rosaleas.jpg',7),(8,'LILI01-Thinking of You.jpg',8),(9,'LILI02-Royal Autumn Lilies.jpg',9),(10,'LILI03-Deluxe Fragrant Stargazer Lilies.jpg',10),(11,'LILI04-Deluxe Joyful Bouquet.jpg',11),(12,'ROS01-Two Dozen Red Roses.jpg',12),(13,'ROS02-18 Pink Pearl Roses.jpg',13),(14,'ROS03-Two Dozen Rainbow Roses.jpg',14),(15,'ROS04-Autumn Days with Felt Leaf Mason Jar & Chocolates.jpg',15),(16,'PF_17_B350_MINIMUM_VA0007_W1_SQ.jpeg',16),(17,'ACC_17_VA0059_W1_SQ.jpeg',16),(18,'PF_17_B350_LAYDOWN_MINIMUM_W1_SQ.jpeg',16);
/*!40000 ALTER TABLE `photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `actualPrice` float DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `detail` varchar(255) DEFAULT NULL,
  `discountedPrice` float DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `wishListId` int(11) DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKjh9bqp1ohx0aqwx0qwkf2x6v7` (`wishListId`),
  KEY `FK6i3ku5n5njmijfxwv43ktj2ki` (`categoryId`),
  CONSTRAINT `FK6i3ku5n5njmijfxwv43ktj2ki` FOREIGN KEY (`categoryId`) REFERENCES `category` (`id`),
  CONSTRAINT `FKjh9bqp1ohx0aqwx0qwkf2x6v7` FOREIGN KEY (`wishListId`) REFERENCES `wishList` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,45,'abc','abc',35,'BIRTHDAY BLOOMS',1,1),(2,50,'abc','abc',40,'PINK BIRTHDAY BOUQUET',2,2),(3,70,NULL,NULL,65,'MULTI-COLORED TULIPS',NULL,3),(4,55,NULL,NULL,50,'BIRTHDAY SMILES',NULL,1),(5,35,NULL,NULL,30,'SUNFLOWER RADIANCE',NULL,4),(6,60,NULL,NULL,65,'BLOOMS OF SUNSHINE',NULL,5),(7,65,NULL,NULL,60,'BLOOMING ROSALEAS',NULL,6),(8,55,NULL,NULL,50,'THINKING OF YOU',NULL,7),(9,45,NULL,NULL,40,'AUTUMN LILIES',NULL,9),(10,50,NULL,NULL,45,'STARGAZER LILIES',NULL,8),(11,40,NULL,NULL,35,'JOYFUL BOUQUET',NULL,7),(12,55,NULL,NULL,50,'RED ROSES',NULL,7),(13,40,NULL,NULL,35,'PINK PEARL ROSES',NULL,7),(14,45,NULL,NULL,40,'RAINBOW ROSES',NULL,7),(15,35,NULL,NULL,30,'AUTUMN DAYS',NULL,9),(16,50,'Stands approximately 16\" tall','Warm, spicy and comforting, this rich bouquet is bold in color and unique in design. Send exceptional orange roses mixed with red alstroemeria and pops of yellow micro poms, for a gift that is truly a fall favorite.',40,'CINNAMON CIDER',NULL,7);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promotion`
--

DROP TABLE IF EXISTS `promotion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `percent` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promotion`
--

LOCK TABLES `promotion` WRITE;
/*!40000 ALTER TABLE `promotion` DISABLE KEYS */;
/*!40000 ALTER TABLE `promotion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating`
--

DROP TABLE IF EXISTS `rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `avgRating` float DEFAULT NULL,
  `fiveStar` int(11) DEFAULT NULL,
  `fourStar` int(11) DEFAULT NULL,
  `oneStar` int(11) DEFAULT NULL,
  `threeStar` int(11) DEFAULT NULL,
  `twoStar` int(11) DEFAULT NULL,
  `productId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKbopcdyjb7wvmebpbr9y1chcr0` (`productId`),
  CONSTRAINT `FKbopcdyjb7wvmebpbr9y1chcr0` FOREIGN KEY (`productId`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating`
--

LOCK TABLES `rating` WRITE;
/*!40000 ALTER TABLE `rating` DISABLE KEYS */;
/*!40000 ALTER TABLE `rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'user'),(2,'admin');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activationCode` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fullName` varchar(255) DEFAULT NULL,
  `isActive` int(11) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `roleId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8yhl7wdo39n3ee04f8rpajces` (`roleId`),
  CONSTRAINT `FK8yhl7wdo39n3ee04f8rpajces` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (5,NULL,'thupta55@gmail.com','Thu Phan',1,'$2a$10$ypLDkAOrE8lQ1wW6s1bg.OWktC6/TUcXrxSG9.n5liSUCL1CL79VK',1),(6,'$2a$10$U/LuYN2V1Oq5brxxHWNqdu6SBMq1WfmP3/VQaXpsf2jS.bm6NoiIi','tori.phananhthu@gmail.com','Tori Phan',1,'$2a$10$YU/Ch8RlFNEcgmAvCfOQ6uL9499qOYJo/nyCtD6Ejyoqb0YxgSTcC',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wishList`
--

DROP TABLE IF EXISTS `wishList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wishList` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKk5weuu4fitmcc8yj6atsb5vfc` (`userId`),
  CONSTRAINT `FKk5weuu4fitmcc8yj6atsb5vfc` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wishList`
--

LOCK TABLES `wishList` WRITE;
/*!40000 ALTER TABLE `wishList` DISABLE KEYS */;
INSERT INTO `wishList` VALUES (1,5),(2,6);
/*!40000 ALTER TABLE `wishList` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-27 17:02:07
